import CanvasController from "./canvas-controller";
import { palette } from "../color";

const maxDrawDist = 3;

export default class DrawController extends CanvasController {

    constructor(id, width, height) {
        super(id, width, height);
        // [ {x, y} ]
        this.graphs = [];
        this.points = [];
        this.pathEndIndex = [];
        this.curUndoIndex = 0;
        // Lengths of the paths after we've undone to certain points
        // this.undoIndexes = 0;
        this.drawing = false;
        this.onDrawingStart = [];
        this.onDrawingEnd = [];
       
        this.canvas.addEventListener('mousedown', () => this.startDrawing());
        this.canvas.addEventListener('touchstart', () => this.startDrawing());

        document.addEventListener('mouseup', () => this.stopDrawing());
        document.addEventListener('touchend', () => this.stopDrawing());

        // Prevent scrolling while we're drawing here
        this.canvas.addEventListener('touchmove', (evt) => evt.preventDefault(), {passive: false});

        document.addEventListener('keydown', (evt) => this.checkKeys(evt));
    }

   
    get path() {
        return this.graphs;
    }

    /**
     * @param {KeyboardEvent} evt
     */
    checkKeys(evt) {
        switch (evt.key.toLowerCase()) {
            case 'z':
                this.undo();
                break;
            case 'y':
                this.redo();
                break;
            case 'c':
                this.clear();
                // this.points = [];
                this.drawing = false;
                break;
            case 'n':
                break;
            case 'p':
                // "print"
                console.log(this.path);
                break;
        }
    }

    startDrawing() {
        this.drawing = true;

        this.onDrawingStart.forEach(fn => fn());
    }

    stopDrawing() {
        if (!this.drawing) {
            return;
        }
        this.drawing = false;
        this.graphs.push(this.points.slice(0, this.points.length));
        this.points = [];
        this.curUndoIndex ++;
        
        this.onDrawingEnd.forEach(fn => fn());
    }

    undo() {
        let newIndex = this.curUndoIndex - 1;
        if (newIndex < 0) {
            newIndex = 0;
        }
        if (newIndex != this.curUndoIndex) {
            this.curUndoIndex = newIndex;
            this.pathEndIndex = this.graphs.pop();

            this.onDrawingEnd.forEach(fn => fn());
        }
    }

    redo() {
        let newIndex = this.curUndoIndex + 1;
        if (newIndex > this.undoIndexes.length - 1) {
            newIndex = this.undoIndexes.length - 1;
        }
        if (newIndex != this.curUndoIndex) {
            this.curUndoIndex = newIndex;
            this.onDrawingEnd.forEach(fn => fn());
        }
   }

    update(dt, mousePosition) {
        if (!mousePosition || !this.drawing) {
            return;
        }

        const canvasPosition = this.canvas.getBoundingClientRect();
        // we have to account for the border here too
        const actualWidth = (canvasPosition.right - canvasPosition.left) - 2;
        // 500 being the 'default' width
        const scale = 500 / actualWidth;
        const point = {
            x: scale * (mousePosition.x - canvasPosition.x),
            y: scale * (mousePosition.y - canvasPosition.y),
        }
        if (this.points.length == 0) {
            this.points.push(point);
            // this.pathEndIndex = this.points.length;
        }
        else {
            // only add it if it's far enough away from the last thing
            // TODO: Switch to using the undo point
            const lastPoint = this.points[this.points.length - 1];
            const dx = point.x - lastPoint.x;
            const dy = point.y - lastPoint.y;
            const sqDist = dx * dx + dy * dy;
            if (sqDist > maxDrawDist * maxDrawDist) {
                this.points.push(point);
                // this.pathEndIndex = this.points.length;
            }
        }
    }

    render() {
        this.clear();
        this.drawPath();
        this.drawPoints();
    }

    drawPath(){
        this.context.beginPath();
        this.context.strokeStyle = palette.pink;
        this.context.lineWidth = 2;
        for (let i = 0; i < this.points.length; i ++) {
            //  console.log(this.points[i])
            if (i == 0) {
                this.context.moveTo(this.points[i].x, this.points[i].y);
            }
            else {
                this.context.lineTo(this.points[i].x, this.points[i].y);
            }
        }
        this.context.closePath();
        this.context.stroke();
    }

    drawPoints() {
        this.context.beginPath();
        this.context.strokeStyle = palette.pink;
        this.context.lineWidth = 2;
        for (let j = 0; j < this.graphs.length; j++){
            for (let i = 0; i < this.graphs[j].length; i ++) {
                if (i == 0) {
                    this.context.moveTo(this.graphs[j][i].x, this.graphs[j][i].y);
                }
                else {
                    this.context.lineTo(this.graphs[j][i].x, this.graphs[j][i].y);
                }
            }
            this.context.closePath();
            this.context.stroke();
        }
    }
}
