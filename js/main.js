import DrawController from './controller/draw-controller.js';
import EpicyclesController from './controller/epicycles-controller.js';
import Conductor from './conductor.js';
import RangeController from './controller/range-controller.js';

let conductor = null;

function init() {
 
    let controllers = [];
    let drawZone, circleZoneSlider;
    if (hasElement('draw-zone')) {
        drawZone = new DrawController('draw-zone');
        controllers.push(drawZone);
    }
    if (hasElement('draw-zone-undo-button')) {
        const undoButton = document.getElementById('draw-zone-undo-button');
        if (drawZone) {
            undoButton.addEventListener('click', () => drawZone.undo());
        }
    }
    if (hasElement('circle-zone-slider')) {
        circleZoneSlider = new RangeController('circle-zone-slider');
        circleZoneSlider.animate = false;
        controllers.push(circleZoneSlider);
    }
    if (hasElement('circle-zone')) {
        let epicycles = new EpicyclesController('circle-zone');
        epicycles.animatePathAmt = false;
        
        if (drawZone) {
            
            drawZone.onDrawingStart.push(() => {
                epicycles.setPath([])
            });
            drawZone.onDrawingEnd.push(() => {

                drawZone.graphs.forEach((path, index) => {
                    epicycles.setPath(path, 1024, 0.01, index);
                })
            });
            // Reset the slider back to 1 to draw the full shape when it changes.
            if (circleZoneSlider) {
                drawZone.onDrawingStart.push(() => {
                    circleZoneSlider.slider.value = 1;
                    epicycles.setFourierAmt(1);
                });
            }
        }
        if (circleZoneSlider) {
            circleZoneSlider.onValueChange.push(val => epicycles.setFourierAmt(val));
        }

        if (hasElement('download-button')) {
            const DownloadButton = document.getElementById('download-button');
            const SvgButton = document.getElementById('svg-button');
            if (epicycles) {
                DownloadButton.addEventListener('click', () => {
                    var name= Math.floor(Math.random() * 101);
                    var newname = name+".png";
                    var path = epicycles.download();
                    document.getElementById("downloadPng").download = newname;
                    document.getElementById("downloadPng").href = path;
                    var potrace = require('potrace');
                    // Tracing
                    var trace = new potrace.Potrace();
                    // You can also pass configuration object to the constructor
                    trace.setParameters({
                        threshold: 128,
                        color: 'black'
                    });
                    trace.loadImage(path, function(err) {
                        if (err) throw err;
                        document.getElementById("svg-zone").innerHTML = trace.getSVG(); // returns SVG document contents
                        document.getElementById("downloadSvg").download = name+".svg";
                        document.getElementById("downloadSvg").href = "data:image/svg+xml;,"+trace.getSVG();
                    });
                    
                });
            
            }
        }
        controllers.push(epicycles);
        
    }

    conductor = new Conductor(controllers);
    conductor.start();
    // To let me play around with things in the console.
    window.conductor = conductor;
}

function sleep(time){
    return new Promise((resolve) => setTimeout(resolve, time));
}

function hasElement(id) {
    return document.getElementById(id) != null;
}

/**
 * Configure the canvases to be able to handle screens with higher dpi.
 *
 * We can only call this once because after that, the width has changed!
 */
function updateCanvasSizes() {
    const pixelRatio = window.devicePixelRatio || 1;
    const canvases = document.getElementsByTagName("canvas");
    for (let canvas of canvases) {
        const width = canvas.width;
        const height = canvas.height;
        canvas.width = width * pixelRatio;
        canvas.height = height * pixelRatio;
        canvas.style.width = width + 'px';
        canvas.style.height = height + 'px';
    }
}

// updateCanvasSizes();
window.onload = init